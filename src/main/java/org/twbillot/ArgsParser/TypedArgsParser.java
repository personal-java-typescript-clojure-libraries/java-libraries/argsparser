package org.twbillot.ArgsParser;

import org.twbillot.ArgsParser.Argument.Argument;
import org.twbillot.ArrayUtils.ArrayUtils;

//TODO: Test relevant argument instance filter
//TODO: Test relevant command-line argument extractor
public class TypedArgsParser<T> extends TemplateArgsParser<T> {
	public TypedArgsParser() { super(); }
	public TypedArgsParser(boolean isStrict) { super(isStrict); }
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public	Argument	filter(
			Argument	arg) {
		try {
			Argument<T,?> casted = (Argument<T,?>) arg;
			return add(casted) ? null : arg;
		} catch (ClassCastException e) {
			return null;
		}
	}
	@SuppressWarnings("rawtypes")
	public	Argument[]	filter(
			Argument[]	args) {
		Argument[] out = new Argument[args.length];
		for (int i = 0; i < args.length; i++) {
			Argument arg = filter(args[i]);
			if (arg != null) { out[i] = arg; }
		}
		return out;
	}
	public	String[]	extract(
			String...	args) {
		String[] out = new String[0];
		parse(args);
		Argument<T,?>[] valids = ArrayUtils.find(
				getArguments(),
				Argument::isValidated);
		@SuppressWarnings("unchecked")
		Argument<T,?>[] waitings = new Argument[0];
		for (String arg : args) {
			if (fullTagged(arg)) {
				Argument<T,?> fullTag = findFull(arg, valids);
				try {
					out = ArrayUtils.add(out, addFullTag(fullTag.getFull()));
					waitings = ArrayUtils.add(waitings, fullTag);
				} catch (NullPointerException ignored) { }
			} else if (shortTagged(arg)) {
				Argument<T,?>[] shortTags = findShort(arg, valids);
				final String EMPTY = "";
				StringBuilder shortTag = new StringBuilder(EMPTY);
				for (Argument<T,?> a : shortTags) {
					shortTag.append(a.getShort());
					waitings = ArrayUtils.add(waitings, a);
				}
				if (EMPTY.equals(shortTag.toString())) {
					out = ArrayUtils.add(out, addShortTag(shortTag.toString()));
				}
			} else {
				if (ArrayUtils.contain(
						waitings, 
						arg, 
						(Argument<T,?> a, String c) -> {
							boolean eq = false;
							try { eq = a.cast(c).equals(a.getValue());
							} catch (Exception e) { System.out.println(e.getMessage()); }
							return eq;
						})) {
					out = ArrayUtils.add(out, arg);
				}
			}
		}
		return out;
	}
	public	static	<U>	void	pass(U target, Argument<U,?>[] objArgs, String[] cmdArgs) {
		pass(target, objArgs, cmdArgs, true);
	}
	public	static	<U>	void	pass(
			U					target, 
			Argument<U,?>[]		objArgs, 
			String[]			cmdArgs, 
			boolean				isStrict) {
		TypedArgsParser<U> parser = new TypedArgsParser<U>(isStrict);
		parser.apply(target, parser.run(objArgs, cmdArgs));
	}
}
