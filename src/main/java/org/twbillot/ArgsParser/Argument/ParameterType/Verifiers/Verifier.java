package org.twbillot.ArgsParser.Argument.ParameterType.Verifiers;

public interface Verifier {
	boolean	verifying(String value);
}
