package org.twbillot.ArgsParser.Argument.ParameterType.Verifiers;

import org.twbillot.ArgsParser.Argument.ParameterType.Verifiers.TextVerifier;

import java.nio.file.InvalidPathException;
import java.nio.file.Paths;

public class PathVerifier extends TextVerifier {
	public	boolean	verifying(
			String	value) {
		if (super.verifying(value)) {
			try { Paths.get(value); return true;
			} catch (InvalidPathException e) { return false; }
		}
		return false;
	}
}
