package org.twbillot.ArgsParser.Argument.ParameterType;

import org.twbillot.ArgsParser.Argument.ParameterType.Verifiers.BooleanVerifier;
import org.twbillot.ArgsParser.Argument.ParameterType.Verifiers.NumberVerifier;
import org.twbillot.ArgsParser.Argument.ParameterType.Verifiers.PathVerifier;
import org.twbillot.ArgsParser.Argument.ParameterType.Verifiers.RangedNumberVerifier;
import org.twbillot.ArgsParser.Argument.ParameterType.Verifiers.TextVerifier;
import org.twbillot.ArgsParser.Argument.ParameterType.Verifiers.Verifier;

import java.util.function.Function;

public enum ParameterType {
	Text			(
						new TextVerifier(), 
						"Text (String)", 
						(String i) -> i),
	Integer			(
						new NumberVerifier<Integer>(
								java.lang.Integer::parseInt),
						"Integer (int)",
			java.lang.Integer::parseInt),
	IntegerPlus		(
						new RangedNumberVerifier<Integer>(
								java.lang.Integer::parseInt,
								0, 
								true, 
								true), 
						"Integer+ (int+)",
			java.lang.Integer::parseInt),
	IntegerMinus	(
						new RangedNumberVerifier<Integer>(
								java.lang.Integer::parseInt,
								0, 
								true, 
								false), 
						"Integer- (int-)",
			java.lang.Integer::parseInt),
	Decimal			(
						new NumberVerifier<Double>(
								Double::parseDouble),
						"Decimal (double)",
			Double::parseDouble),
	DecimalPlus		(
						new RangedNumberVerifier<Double>(
								Double::parseDouble,
								0.0d, 
								true, 
								true), 
						"Decimal+ (double+)",
			Double::parseDouble),
	DecimalMinus	(
						new RangedNumberVerifier<Double>(
								Double::parseDouble,
								0.0d, 
								true, 
								false), 
						"Decimal- (double-)",
			Double::parseDouble),
	Boolean			(
						new BooleanVerifier(), 
						"boolean",
			java.lang.Boolean::parseBoolean),
	Path			(
						new PathVerifier(), 
						"Path (String)", 
						(String i) -> i);
	private	final	Verifier					verif;
	private	final	String						toStr;
	private	final	Function<String, Object>	caster;
	ParameterType(Verifier verif, String toStr, Function<String, Object> caster) {
		this.verif = verif;
		this.toStr = toStr;
		this.caster = caster;
	}
	public	boolean	verify(String value) { return verif.verifying(value); }
	public	Object	cast(String value) {
		if (verify(value)) { return caster.apply(value);
		} else { throw new IllegalArgumentException(value + " incompatible with " + toStr); }
	}
	public	String	toString() { return toStr; }
}
