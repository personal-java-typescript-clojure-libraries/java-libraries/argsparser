package org.twbillot.ArgsParser.Argument.ParameterType.Verifiers;

import java.util.function.Function;

public class RangedNumberVerifier<T extends Number> extends NumberVerifier<T> {
	private	final	T		threshold;
	private	final	boolean	equal;
	private	final	boolean	upper;
	public RangedNumberVerifier(
			Function<String, T> caster, 
			T					threshold, 
			boolean				equal, 
			boolean				upper) {
		super(caster);
		this.threshold = threshold;
		this.equal = equal;
		this.upper = upper;
	}
	public	boolean	verifying(
			String	value) {
		if (super.verifying(value)) {
			T val = caster.apply(value);
			if (equal && threshold.doubleValue() == val.doubleValue()) { return true; }
			if (upper && threshold.doubleValue() < val.doubleValue()) { return true; }
			return !upper && val.doubleValue() < threshold.doubleValue();
		}
		return false;
	}
}
