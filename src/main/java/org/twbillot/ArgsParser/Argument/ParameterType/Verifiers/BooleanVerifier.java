package org.twbillot.ArgsParser.Argument.ParameterType.Verifiers;

import org.twbillot.ArgsParser.Argument.ParameterType.Verifiers.Verifier;

public class BooleanVerifier implements Verifier {
	public	boolean	verifying(String value) { return Boolean.parseBoolean(value); }
}
