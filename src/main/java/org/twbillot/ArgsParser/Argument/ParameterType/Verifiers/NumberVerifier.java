package org.twbillot.ArgsParser.Argument.ParameterType.Verifiers;

import org.twbillot.ArgsParser.Argument.ParameterType.Verifiers.Verifier;

import java.util.function.Function;

public class NumberVerifier<T extends Number> implements Verifier {
	protected	final	Function<String, T>	caster;
	public NumberVerifier(Function<String, T> caster) { this.caster = caster; }
	public	boolean	verifying(String value) {
		try { caster.apply(value); return true; } catch (NumberFormatException e) { return false; }
	}
}
