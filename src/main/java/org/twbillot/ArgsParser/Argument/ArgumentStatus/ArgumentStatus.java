package org.twbillot.ArgsParser.Argument.ArgumentStatus;

public enum ArgumentStatus {
	Initialized	("Initialized"), 
	Waiting		("Waiting for argument parameter"), 
	Validated	("Validated"), 
	Rejected	("Rejected");
	private	final	String	toStr;
	ArgumentStatus(String toStr) { this.toStr = toStr; }
	public	String	toString() { return toStr; }
}
