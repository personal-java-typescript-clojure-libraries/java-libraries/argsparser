package org.twbillot.ArgsParser.Argument;

import java.util.function.BiConsumer;

import org.twbillot.ArgsParser.Argument.ArgumentStatus.ArgumentStatus;
import org.twbillot.ArgsParser.Argument.ParameterType.ParameterType;

//TODO: Multiple full tags, multiple shorthands, multiple Parameter values
public class Argument<T, U> {
	private	final	String			name;
	private	final	String			desc;
	private	final	String			full;//String[] fulls
	private	final	String			shorthand;//String[] shorthands
	private	final	boolean			required;
	private	final	boolean			param;
	private	final	ParameterType	paramType;//New ParameterType ?
	private	final	boolean			paramRequired;
	private	final	U				paramDefault;
	private	final	U[]				paramExcluded;
	private	U					paramValue;
	private	ArgumentStatus		status;
	private	BiConsumer<T, U>	apply;
	public Argument(
			String			name, 
			String			desc, 
			String			full, 
			String			shorthand, 
			boolean			required, 
			boolean			param, 
			ParameterType	paramType, 
			boolean			paramRequired, 
			U				paramDefault, 
			U[]				paramExcluded, 
			BiConsumer<T, U>	apply) {
		this.name = name;
		this.desc = desc;
		this.full = full;
		this.shorthand = shorthand;
		this.required = required;
		this.param = param;
		this.paramType = paramType;
		this.paramRequired = paramRequired;
		this.paramDefault = paramDefault;
		this.paramExcluded = paramExcluded;
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
		this.apply = apply;
	}
	public Argument(
			String			name, 
			String			desc, 
			String			full, 
			String			shorthand, 
			boolean			required, 
			boolean			param, 
			ParameterType	paramType, 
			boolean			paramRequired, 
			U				paramDefault, 
			BiConsumer<T, U>	apply) {
		this.name = name;
		this.desc = desc;
		this.full = full;
		this.shorthand = shorthand;
		this.required = required;
		this.param = param;
		this.paramType = paramType;
		this.paramRequired = paramRequired;
		this.paramDefault = paramDefault;
		this.paramExcluded = DEFAULT_PARAM_EXCLUDED();
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
		this.apply = apply;
	}
	public Argument(
			String			full, 
			String			shorthand, 
			boolean			required, 
			boolean			param, 
			ParameterType	paramType, 
			boolean			paramRequired, 
			BiConsumer<T, U>	apply) {
		this.name = DEFAULT_NAME();
		this.desc = DEFAULT_DESC();
		this.full = full;
		this.shorthand = shorthand;
		this.required = required;
		this.param = param;
		this.paramType = paramType;
		this.paramRequired = paramRequired;
		this.paramDefault = DEFAULT_PARAM_DEFAULT();
		this.paramExcluded = DEFAULT_PARAM_EXCLUDED();
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
		this.apply = apply;
	}
	public Argument(
			String			full, 
			String			shorthand, 
			boolean			required, 
			boolean			param, 
			boolean			paramRequired, 
			BiConsumer<T, U>	apply) {
		this.name = DEFAULT_NAME();
		this.desc = DEFAULT_DESC();
		this.full = full;
		this.shorthand = shorthand;
		this.required = required;
		this.param = param;
		this.paramType = DEFAULT_PARAM_TYPE();
		this.paramRequired = paramRequired;
		this.paramDefault = DEFAULT_PARAM_DEFAULT();
		this.paramExcluded = DEFAULT_PARAM_EXCLUDED();
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
		this.apply = apply;
	}
	public Argument(
			String			full, 
			String			shorthand, 
			boolean			required, 
			boolean			param, 
			ParameterType	paramType, 
			BiConsumer<T, U>	apply) {
		this.name = DEFAULT_NAME();
		this.desc = DEFAULT_DESC();
		this.full = full;
		this.shorthand = shorthand;
		this.required = required;
		this.param = param;
		this.paramType = paramType;
		this.paramRequired = DEFAULT_PARAM_REQUIRED();
		this.paramDefault = DEFAULT_PARAM_DEFAULT();
		this.paramExcluded = DEFAULT_PARAM_EXCLUDED();
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
		this.apply = apply;
	}
	public Argument(
			String			full, 
			String			shorthand, 
			boolean			required, 
			boolean			param, 
			BiConsumer<T, U>	apply) {
		this.name = DEFAULT_NAME();
		this.desc = DEFAULT_DESC();
		this.full = full;
		this.shorthand = shorthand;
		this.required = required;
		this.param = param;
		this.paramType = DEFAULT_PARAM_TYPE();
		this.paramRequired = DEFAULT_PARAM_REQUIRED();
		this.paramDefault = DEFAULT_PARAM_DEFAULT();
		this.paramExcluded = DEFAULT_PARAM_EXCLUDED();
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
		this.apply = apply;
	}
	public Argument(
			String			full, 
			String			shorthand, 
			boolean			required, 
			BiConsumer<T, U>	apply) {
		this.name = DEFAULT_NAME();
		this.desc = DEFAULT_DESC();
		this.full = full;
		this.shorthand = shorthand;
		this.required = required;
		this.param = DEFAULT_PARAM();
		this.paramType = DEFAULT_PARAM_TYPE();
		this.paramRequired = DEFAULT_PARAM_REQUIRED();
		this.paramDefault = DEFAULT_PARAM_DEFAULT();
		this.paramExcluded = DEFAULT_PARAM_EXCLUDED();
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
		this.apply = apply;
	}
	public Argument(
			String			full, 
			String			shorthand, 
			BiConsumer<T, U>	apply) {
		this.name = DEFAULT_NAME();
		this.desc = DEFAULT_DESC();
		this.full = full;
		this.shorthand = shorthand;
		this.required = DEFAULT_REQUIRED();
		this.param = DEFAULT_PARAM();
		this.paramType = DEFAULT_PARAM_TYPE();
		this.paramRequired = DEFAULT_PARAM_REQUIRED();
		this.paramDefault = DEFAULT_PARAM_DEFAULT();
		this.paramExcluded = DEFAULT_PARAM_EXCLUDED();
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
		this.apply = apply;
	}
	public Argument(
			String			name, 
			String			desc, 
			String			full, 
			String			shorthand, 
			boolean			required, 
			boolean			param, 
			ParameterType	paramType, 
			boolean			paramRequired, 
			U				paramDefault, 
			U[]				paramExcluded) {
		this.name = name;
		this.desc = desc;
		this.full = full;
		this.shorthand = shorthand;
		this.required = required;
		this.param = param;
		this.paramType = paramType;
		this.paramRequired = paramRequired;
		this.paramDefault = paramDefault;
		this.paramExcluded = paramExcluded;
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
		this.apply = DEFAULT_APPLY();
	}
	public Argument(
			String			name, 
			String			desc, 
			String			full, 
			String			shorthand, 
			boolean			required, 
			boolean			param, 
			ParameterType	paramType, 
			boolean			paramRequired, 
			U				paramDefault) {
		this.name = name;
		this.desc = desc;
		this.full = full;
		this.shorthand = shorthand;
		this.required = required;
		this.param = param;
		this.paramType = paramType;
		this.paramRequired = paramRequired;
		this.paramDefault = paramDefault;
		this.paramExcluded = DEFAULT_PARAM_EXCLUDED();
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
	}
	public Argument(
			String			full, 
			String			shorthand, 
			boolean			required, 
			boolean			param, 
			ParameterType	paramType, 
			boolean			paramRequired) {
		this.name = DEFAULT_NAME();
		this.desc = DEFAULT_DESC();
		this.full = full;
		this.shorthand = shorthand;
		this.required = required;
		this.param = param;
		this.paramType = paramType;
		this.paramRequired = paramRequired;
		this.paramDefault = DEFAULT_PARAM_DEFAULT();
		this.paramExcluded = DEFAULT_PARAM_EXCLUDED();
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
	}
	public Argument(
			String			full, 
			String			shorthand, 
			boolean			required, 
			boolean			param, 
			boolean			paramRequired) {
		this.name = DEFAULT_NAME();
		this.desc = DEFAULT_DESC();
		this.full = full;
		this.shorthand = shorthand;
		this.required = required;
		this.param = param;
		this.paramType = DEFAULT_PARAM_TYPE();
		this.paramRequired = paramRequired;
		this.paramDefault = DEFAULT_PARAM_DEFAULT();
		this.paramExcluded = DEFAULT_PARAM_EXCLUDED();
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
	}
	public Argument(
			String			full, 
			String			shorthand, 
			boolean			required, 
			boolean			param, 
			ParameterType	paramType) {
		this.name = DEFAULT_NAME();
		this.desc = DEFAULT_DESC();
		this.full = full;
		this.shorthand = shorthand;
		this.required = required;
		this.param = param;
		this.paramType = paramType;
		this.paramRequired = DEFAULT_PARAM_REQUIRED();
		this.paramDefault = DEFAULT_PARAM_DEFAULT();
		this.paramExcluded = DEFAULT_PARAM_EXCLUDED();
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
	}
	public Argument(
			String			full, 
			String			shorthand, 
			boolean			required, 
			boolean			param) {
		this.name = DEFAULT_NAME();
		this.desc = DEFAULT_DESC();
		this.full = full;
		this.shorthand = shorthand;
		this.required = required;
		this.param = param;
		this.paramType = DEFAULT_PARAM_TYPE();
		this.paramRequired = DEFAULT_PARAM_REQUIRED();
		this.paramDefault = DEFAULT_PARAM_DEFAULT();
		this.paramExcluded = DEFAULT_PARAM_EXCLUDED();
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
	}
	public Argument(
			String			full, 
			String			shorthand, 
			boolean			required) {
		this.name = DEFAULT_NAME();
		this.desc = DEFAULT_DESC();
		this.full = full;
		this.shorthand = shorthand;
		this.required = required;
		this.param = DEFAULT_PARAM();
		this.paramType = DEFAULT_PARAM_TYPE();
		this.paramRequired = DEFAULT_PARAM_REQUIRED();
		this.paramDefault = DEFAULT_PARAM_DEFAULT();
		this.paramExcluded = DEFAULT_PARAM_EXCLUDED();
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
	}
	public Argument(
			String			full, 
			String			shorthand) {
		this.name = DEFAULT_NAME();
		this.desc = DEFAULT_DESC();
		this.full = full;
		this.shorthand = shorthand;
		this.required = DEFAULT_REQUIRED();
		this.param = DEFAULT_PARAM();
		this.paramType = DEFAULT_PARAM_TYPE();
		this.paramRequired = DEFAULT_PARAM_REQUIRED();
		this.paramDefault = DEFAULT_PARAM_DEFAULT();
		this.paramExcluded = DEFAULT_PARAM_EXCLUDED();
		this.paramValue = this.paramDefault;
		this.status = INIT_STATUS();
	}
	private String			DEFAULT_NAME() { return "Missing name: To implement"; }
	private String			DEFAULT_DESC() { return "Missing description: To implement"; }
	private boolean			DEFAULT_REQUIRED() { return false; }
	private boolean			DEFAULT_PARAM() { return false; }
	private ParameterType	DEFAULT_PARAM_TYPE() { return ParameterType.Text; }
	private boolean			DEFAULT_PARAM_REQUIRED() { return false; }
	private U				DEFAULT_PARAM_DEFAULT() { return null; }
	private U[]				DEFAULT_PARAM_EXCLUDED() { return null; }
	private ArgumentStatus	INIT_STATUS() { return ArgumentStatus.Initialized; }
	@SuppressWarnings({"unchecked","unused"})
	private BiConsumer<T, U>		CONFIRM_APPLY(
			Object	candidate) 
			throws	IllegalArgumentException {
		if (candidate instanceof BiConsumer<?, ?>) {
			try {
				return (BiConsumer<T, U>) candidate;
			} catch (Exception e) {
				throw new IllegalArgumentException("Non-usable BiConsumer<T, U>");
			}
		}
		throw new IllegalArgumentException("Non-usable BiConsumer<T, U>");
	}
	private BiConsumer<T, U>		DEFAULT_APPLY() { return (T target, U paramValue) -> { }; }
	public	String			getShort() { return shorthand; }
	public	String			getFull() { return full; }
	public	U				getValue() { return paramValue; }
	public	ArgumentStatus	getStatus() { return status; }
	public	void	setValue(U value) { paramValue = value; }
	public	void	setValueFromString(String value) throws Exception { paramValue = cast(value); }
	public	void	setStatus(ArgumentStatus status) { this.status = status; }
	public	void	setApply(BiConsumer<T, U> apply) { this.apply = apply; }
	public	boolean	isWaiting() { return status.equals(ArgumentStatus.Waiting); }
	public	boolean	isValidated() { return status.equals(ArgumentStatus.Validated); }
	public	boolean	isRejected() { return status.equals(ArgumentStatus.Rejected); }
	public	boolean	isRequired() { return required; }
	public	boolean	hasParam() { return param; }
	public	boolean	hasParamRequired() { return paramRequired; }
	public	boolean	hasParamDefault() { return paramDefault != null; }
	public	String	resumeArgument() {
		String out = "Name: " + name + "\n" + 
				"Full tag: " + full + "\n" + 
				"Short tag: " + shorthand + "\n" + 
				"Required: " + required + "\n" + 
				"Parameterizable: " + param + "\n" + 
				"Parameter type: " + paramType.toString() + "\n" + 
				"Parameter required: " + paramRequired + "\n" + 
				"Default parameter: " + (paramDefault != null ? paramDefault.toString() : "[NULL]") + "\n";
		if (paramExcluded != null) {
			String[] exclStrArr = new String[paramExcluded.length];
			for (int i = 0; i < paramExcluded.length; i++) { exclStrArr[i] = paramExcluded[i].toString(); }
			out += "Parameter excluded: [ " + String.join(", ", exclStrArr) + " ]\n";
		} else { out += "Parameter excluded: [NULL]\n"; }
		out += "Parameter value: " + (paramValue != null ? paramValue.toString() : "[NULL]") + "\n" + 
				"Status: " + status.toString() + "\n" + 
				"Description: " + desc + "\n";
		return out;
	}
	private	void	waitFor() { status = ArgumentStatus.Waiting; }
	private	void	valid() { status = ArgumentStatus.Validated; }
	private	void	reject() { status = ArgumentStatus.Rejected; }
	public	void	nextStatus() { if (! param) { valid(); } else { waitFor(); } }
	private	boolean	in(
			U		value, 
			U[]		excluded) {
		boolean found = false;
		int i = 0;
		while (! found && i < excluded.length) {
			found = value.equals(excluded[i]);
			i++;
		}
		return found;
	}
	private	void	canBeParameterized(
			String	name) 
			throws	IllegalArgumentException {
		if (! hasParam()) {
			throw new IllegalArgumentException(name + " doesn't accept parameter");
		}
	}
	private	U	cast(
			ParameterType	type, 
			String			value, 
			U[]				excluded) {
		@SuppressWarnings("unchecked")
		U out = (U) type.cast(value);
		if (! in(out, excluded)) { return out;
		} else { throw new IllegalArgumentException(value + " in Exclusion List"); }
	}
	public	U	cast(String value)	throws	Exception {
		return cast(paramType, value, paramExcluded);
	}
	public	boolean	addValue(String value) {
		boolean added = false;
		try {
			canBeParameterized(name);
			paramValue = cast(paramType, value, paramExcluded);
			valid();
			added = true;
		} catch (Exception e) { System.out.println(e.getMessage()); }
		return added;
	}
	private	void	finalizeWaiting() {
		// Waiting with non-required Parameter => Valid
		if (isWaiting() && hasParam() && ! hasParamRequired()) { valid();
		// Waiting with required Parameter with Default => Valid
		} else if (isWaiting() && hasParam() && hasParamRequired() && hasParamDefault()) { valid();
		// Waiting with required Parameter without Default => Reject
		} else if (isWaiting() && hasParam() && hasParamRequired() && ! hasParamDefault()) { reject(); }
	}
	private	void	finalizeRequired() {
		// Non-validate required => Reject
		if (isRequired() && ! isValidated()) { reject(); }
	}
	public	void	finalizeArgument() {
		finalizeWaiting();
		finalizeRequired();
	}
	public	void	apply(T target) { apply.accept(target, paramValue); }
}
