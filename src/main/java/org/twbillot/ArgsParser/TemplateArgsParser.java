package org.twbillot.ArgsParser;

import org.twbillot.ArgsParser.Argument.Argument;
import org.twbillot.ArrayUtils.ArrayUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

//TODO: Multiple full tags, multiple shorthands, multiple Parameter values handling
abstract class TemplateArgsParser<T> {
	private	final	String		SHORT_TAG;
	private	final	String		FULL_TAG;
	private	final	boolean		STRICT;
	private	Argument<T,?>[]	arguments;
	@SuppressWarnings("unchecked")
	public TemplateArgsParser() {
		SHORT_TAG = "-";
		FULL_TAG = "--";
		STRICT = true;
		arguments = new Argument[0];
	}
	@SuppressWarnings("unchecked")
	public TemplateArgsParser(boolean isStrict) {
		SHORT_TAG = "-";
		FULL_TAG = "--";
		STRICT = isStrict;
		arguments = new Argument[0];
	}
	public	Argument<T,?>[]	getArguments() { return arguments; }
	private	boolean	caseSensibleMatch(String ref, String mob) { return ref.equals(mob); }
	private	boolean	caseInsensibleMatch(String ref, String mob) { return ref.toLowerCase().equals(mob.toLowerCase()); }
	private	boolean	matchShort(String target, Argument<T,?> arg) { return caseSensibleMatch(target, arg.getShort()); }
	private	boolean	matchFull(String target, Argument<T,?> arg) { return caseInsensibleMatch(target, arg.getFull()); }
	private	<U>	Map<String, U>	updateMapWithChar(
			String				str, 
			U					val) {
		return updateMapWithChar(
				new HashMap<String, U>(), 
				str, 
				val);
	}
	private	<U>	Map<String, U>	updateMapWithChar(
			Map<String, U>		map, 
			String				str, 
			U					val) {
		final String sep = "";
		for (String c : str.split(sep)) { map.put(c, val); }
		return map;
	}
	/*private	<U>	U[]	mapToArray(
			String				str, 
			Map<String, U>		map) {
		U[] newArr = ArrayUtils.newArray(Argument.class, map.size());
		final String sep = "";
		String[] chars = str.split(sep);
		for (int i = 0; i < chars.length; i++) {
			String k = chars[i];
			if (map.containsKey(k)) { newArr[i] = map.get(k); }
		}
		return newArr;
	}*/
	private	boolean				matchCombinedShort(
			String				target, 
			Argument<T,?>[]		arr) {
		boolean found = false;
		Map<String, Boolean> matched = updateMapWithChar(target, false);
		if (1 < target.length()) {
			int i = 0;
			while (! found && i < arr.length) {
				String mobile = arr[i].getShort();
				if (target.contains(mobile)) { matched = updateMapWithChar(matched, mobile, true); }
				found = ! matched.containsValue(false);
				i++;
			}
		}
		return found;
	}
	private	boolean			find(
			String			target, 
			Argument<T,?>[]	arr) {
		boolean found = false;
		int i = 0;
		while (! found && i < arr.length) {
			found = matchShort(target, arr[i]) || matchFull(target, arr[i]);
			i++;
		}
		if (! found) { found = matchCombinedShort(target, arr); }
		return found;
	}
	@SuppressWarnings("unused")
	private	Argument<T,?>	searchFirstInAll(
			String			target, 
			Argument<T,?>[]	arr) {
		Argument<T,?> out = null;
		boolean found = false;
		int i = 0;
		while (! found && i < arr.length) {
			if (matchShort(target, arr[i]) || matchFull(target, arr[i])) {
				out = arr[i];
				found = true;
			}
			i++;
		}
		return out;
	}
	private	Map<String, Argument<T,?>>	mapCombination(
			String						target, 
			Argument<T,?>[]				arr) {
		Map<String, Argument<T,?>> matched = updateMapWithChar(target, null);
		if (1 < target.length()) {
			int i = 0;
			while (matched.containsValue(null) && i < arr.length) {
				Argument<T,?> mobile = arr[i];
				if (target.contains(mobile.getShort())) {
					matched = updateMapWithChar(matched, mobile.getShort(), mobile);
				}
				i++;
			}
		}
		return matched;
	}
	@SuppressWarnings("unchecked")
	private	Argument<T,?>[]				arrayfyCombination(
			String						target, 
			Map<String, Argument<T,?>>	map) {
		Argument<T,?>[] newArr = new Argument[map.size()]; //ArrayUtils.new(Argument.class);
		final String sep = "";
		String[] chars = target.split(sep);
		for (int i = 0; i < chars.length; i++) {
			String k = chars[i];
			if (map.containsKey(k)) { newArr[i] = map.get(k); }
		}
		return newArr;
	}
	private	Argument<T,?>[]	searchCombination(
			String			target, 
			Argument<T,?>[]	arr) {
		return ArrayUtils.find(
				ArrayUtils.unique(
						arrayfyCombination( //mapToArray(
								target, 
								mapCombination(
										target, 
										arr))),
				Objects::nonNull);
	}
	@SuppressWarnings("unchecked")
	private	Argument<T,?>[]	searchInShort(
			String			target, 
			Argument<T,?>[]	arr) {
		Argument<T,?>[] out = new Argument[0];
		boolean found = false;
		int i = 0;
		while (! found && i < arr.length) {
			if (matchShort(target, arr[i])) {
				out = ArrayUtils.add(out, arr[i]);
				found = true;
			}
			i++;
		}
		if (! found) { out = ArrayUtils.add(out, searchCombination(target, arr)); }
		return out;
	}
	private	Argument<T,?>	searchInFull(
			String			target, 
			Argument<T,?>[]	arr) {
		Argument<T,?> out = null;
		boolean found = false;
		int i = 0;
		while (! found && i < arr.length) {
			Argument<T,?> mobile = arr[i];
			if (matchFull(target, mobile)) { out = mobile; found = true; }
			i++;
		}
		return out;
	}
	public	boolean		add(
			Argument<T,?>	arg) {
		boolean out = ! (find(arg.getShort(), arguments) || find(arg.getFull(), arguments));
		if (out) { arguments = ArrayUtils.add(arguments, arg); }
		return out;
	}
	@SuppressWarnings("unchecked")
	public	boolean[]	add(
			Argument<T,?>...	args) {
		boolean[] out = new boolean[args.length];
		for (int i = 0; i < args.length; i++) { out[i] = add(args[i]); }
		return out;
	}
	protected	boolean	shortTagged(String arg) { return arg.startsWith(SHORT_TAG); }
	protected	boolean	fullTagged(String arg) { return arg.startsWith(FULL_TAG); }
	private	String		stripShortTag(String arg) { return arg.substring(1); }
	private	String		stripFullTag(String arg) { return arg.substring(2); }
	protected	String	addShortTag(String arg) { return SHORT_TAG + arg; }
	protected	String	addFullTag(String arg) { return FULL_TAG + arg; }
	private		boolean		isStrictJudge() { return STRICT; }
	protected	Argument<T,?>[]	findShort(
			String			cmd, 
			Argument<T,?>[]	args) {
		return searchInShort(stripShortTag(cmd), args);
	}
	protected	Argument<T,?>	findFull(
			String			cmd, 
			Argument<T,?>[]	args) {
		return searchInFull(stripFullTag(cmd), args);
	}
	private	Argument<T,?>[]	treatIfFound(
			Argument<T,?>	val, 
			Argument<T,?>[]	waiting) {
		if (val != null) {
			val.nextStatus();
			waiting = ArrayUtils.shift(waiting, val);
		}
		return waiting;
	}
	private	Argument<T,?>[]	treatIfFound(
			Argument<T,?>[]	vals, 
			Argument<T,?>[]	waiting) {
		for (int i = 0; i < vals.length; i++) {
			Argument<T,?> val = vals[i]; 
			val.nextStatus();
			waiting = ArrayUtils.addAt(waiting, val, i);
		}
		return waiting;
	}
	private	Argument<T,?>[]	assignToWaitingLine(
			String			val, 
			Argument<T,?>[]	waiting) {
		boolean added = false;
		int i = 0;
		while (! added && i < waiting.length) {
			if (waiting[i].addValue(val)) {
				waiting = ArrayUtils.removeAt(waiting, i);
				added = true;
			}
		}
		return waiting;
	}
	private	void			strictJudgement(
			Argument<T,?>	arg) 
			throws			IllegalArgumentException {
		if (isStrictJudge() && arg.isRejected()) {
			throw new IllegalArgumentException(arg.resumeArgument());
		}
	}
	private	void			judge(
			Argument<T,?>[]	args)
			throws			IllegalArgumentException {
		for (Argument<T,?> arg : args) {
			arg.finalizeArgument();
			strictJudgement(arg);
		}
	}
	@SuppressWarnings("unchecked")
	public	void		parse(
			String...	args) 
			throws		IllegalArgumentException {
		Argument<T,?>[] waitingLine = new Argument[0];
		for (String arg : args) {
			if (fullTagged(arg)) {
				//waitingLine = treatIfFound(searchInFull(stripFullTag(arg), arguments), waitingLine);
				waitingLine = treatIfFound(findFull(arg, arguments), waitingLine);
			} else if (shortTagged(arg)) {
				//waitingLine = treatIfFound(searchInShort(stripShortTag(arg), arguments), waitingLine);
				waitingLine = treatIfFound(findShort(arg, arguments), waitingLine);
			} else {
				waitingLine = assignToWaitingLine(arg, waitingLine);
			}
		}
		judge(arguments);
	}
	private	Argument<T,?>[]	registerIfValid(Argument<T,?>[] args, Argument<T,?> val) {
		return val.isValidated() ? ArrayUtils.add(args, val) : args;
	}
	@SuppressWarnings("unchecked")
	public	Argument<T,?>[]	retrieve() {
		Argument<T,?>[] out = new Argument[0];
		for (Argument<T,?> arg : arguments) { out = registerIfValid(out, arg); }
		return out;
	}
	public	Argument<T,?>[]	run(String[] cmdArgs) { parse(cmdArgs); return retrieve(); }
	public	Argument<T,?>[]	run(Argument<T,?>[] objArgs, String[] cmdArgs) { add(objArgs); return run(cmdArgs); }
	public	void		apply(T target) { apply(target, retrieve()); }
	public	void		apply(T target, Argument<T,?>[] args) { for (Argument<T,?> arg : args) { arg.apply(target); } }
}
