package org.twbillot.ArgsParser;

import org.twbillot.ArgsParser.Argument.Argument;

public class ArgsParser extends TemplateArgsParser<Object> {
	public ArgsParser() { super(); }
	public ArgsParser(boolean isStrict) { super(isStrict); }
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public	boolean		add(Argument arg) { return super.add((Argument<Object,?>) arg); }
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public	boolean[]	add(Argument... args) {
		boolean[] out = new boolean[args.length];
		for (int i = 0; i < args.length; i++) { out[i] = super.add((Argument<Object,?>) args[i]); }
		return out;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public	Argument<Object,?>[]	run(Argument[] objArgs, String[] cmdArgs) {
		super.add(objArgs);
		return run(cmdArgs);
	}
	@SuppressWarnings("rawtypes")
	public	static	void	pass(Object target, Argument[] objArgs, String[] cmdArgs) {
		pass(target, objArgs, cmdArgs, true);
	}
	@SuppressWarnings({ "rawtypes"})
	public	static	void	pass(
			Object			target, 
			Argument[]		objArgs, 
			String[]		cmdArgs, 
			boolean			isStrict) {
		ArgsParser parser = new ArgsParser(isStrict);
		parser.apply(target, parser.run(objArgs, cmdArgs));
	}
}
