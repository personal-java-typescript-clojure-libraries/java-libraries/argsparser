module org.twbillot.ArgsParser {
	requires transitive org.twbillot.ArrayUtils;
	exports org.twbillot.ArgsParser;
	exports org.twbillot.ArgsParser.Argument;
	exports org.twbillot.ArgsParser.Argument.ArgumentStatus;
	exports org.twbillot.ArgsParser.Argument.ParameterType;
}